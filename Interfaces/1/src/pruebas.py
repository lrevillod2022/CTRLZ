from utils import *
from cv2 import moments
from numpy import asarray

def main():

    img = Image.open('./imgs/a.png')
    src = asarray(img)

    b_img = to_binary_region1(src)

    M = moments(b_img)

    print(Hu_3(b_img))
    print(((M['nu30'] - (3 * M['nu12']))** 2) + ((3 * M['nu21']) - M['nu03']) **2)


if __name__ == '__main__':
    main()