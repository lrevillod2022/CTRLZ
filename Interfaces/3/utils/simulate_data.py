#!/usr/bin/python3
from serial import Serial
from time import sleep
from random import randint
import sys


def make_data(id_station):
    n_station = id_station
    n_mp01 = randint(5, 100)
    n_mp25 = randint(5, 100)
    n_mp10 = randint(5, 100)
    n_hist03 = randint(10, 10000)
    n_hist05 = randint(10, 10000)
    n_hist01 = randint(10, 10000)
    n_hist25 = randint(10, 10000)
    n_hist50 = randint(10, 10000)
    n_hist10 = randint(10, 10000)
    temp = randint(15, 25)
    hume = randint(20, 35)

    s_frame = "%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d" % (
        n_station, n_mp01, n_mp25, n_mp10, n_hist03, n_hist05, n_hist01, n_hist25, n_hist50, n_hist10, temp, hume)

    return s_frame


def main():

    if len(sys.argv) < 2:
        print('Uso: python3 %s <port>' % __file__[47:], file=sys.stderr)
        exit(-1)

    ser = Serial(sys.argv[1])

    ser.flush()

    station = 1
    while 'a' < 'b':

        s_frame = make_data(station)
        ser.write(s_frame.encode())
        print(s_frame)
        station += 1
        if station == 6:
            station = 1
        sleep(0.1)


if __name__ == '__main__':
    main()
