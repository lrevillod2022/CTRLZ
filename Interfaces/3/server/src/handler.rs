use crate::model::DataModel;

use crate::AppState;
use actix_web::{post, web, HttpResponse, Responder};

#[post("/data")]
async fn get_data(body: web::Json<DataModel>, data: web::Data<AppState>) -> impl Responder {
    let _ = sqlx::query(
        r#"INSERT INTO sensores
        (station, mp01, mp25, mp10, hist03, hist05, hist01, hist25, hist50, hist10, temp, hume)
        VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"#,
    )
    .bind(body.station.to_string())
    .bind(body.mp01.to_string())
    .bind(body.mp25.to_string())
    .bind(body.mp10.to_string())
    .bind(body.hist03.to_string())
    .bind(body.hist05.to_string())
    .bind(body.hist01.to_string())
    .bind(body.hist25.to_string())
    .bind(body.hist50.to_string())
    .bind(body.hist10.to_string())
    .bind(body.temp.to_string())
    .bind(body.hum.to_string())
    .execute(&data.db)
    .await
    .map_err(|err: sqlx::Error| err.to_string());

    println!("{:#?}", body);

    HttpResponse::Ok().body("Todo Ok")
}
