use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize, Serialize)]
pub struct DataModel {
    pub station: u8,
    pub mp01: u8,
    pub mp25: u8,
    pub mp10: u8,
    pub hist03: u16,
    pub hist05: u16,
    pub hist01: u16,
    pub hist25: u16,
    pub hist50: u16,
    pub hist10: u16,
    pub temp: u8,
    pub hum: u8,
}
