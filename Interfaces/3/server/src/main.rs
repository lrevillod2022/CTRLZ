mod handler;
mod model;

use actix_cors::Cors;
use actix_web::{web, App, HttpServer};
use dotenv::dotenv;
use sqlx::{mysql::MySql, Pool};

pub struct AppState {
    db: Pool<MySql>,
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    dotenv().ok();

    let database_url = std::env::var("DATABASE_URL").expect("DATABASE_URL debe estar seteada");

    let pool = Pool::<MySql>::connect(&database_url)
        .await
        .expect("No se pudo conectar a la base de datos");

    println!("Iniciando prototipo de servidor que recepciona datos de material particulado");
    HttpServer::new(move || {
        let cors = Cors::default()
            .allow_any_origin()
            .allowed_methods(vec!["POST"])
            .allow_any_header();
        App::new()
            .app_data(web::Data::new(AppState { db: pool.clone() }))
            .service(handler::get_data)
            .wrap(cors)
    })
    .bind(("0.0.0.0", 7070))?
    .run()
    .await
}
