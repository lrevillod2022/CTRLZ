unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, ExtCtrls,
  ComCtrls, LazSerial, TAGraph, TASeries, TAChartCombos, fphttpclient,
  opensslsockets, fpjson, jsonparser, Types, TADrawUtils, TACustomSeries, strutils;

type

  { TForm1 }

  TForm1 = class(TForm)
    Chart1: TChart;
    SerieMp01: TLineSeries;
    SerieMp25: TLineSeries;
    SerieMp10: TLineSeries;
    Chart2: TChart;
    Histograma: TBarSeries;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    GroupBox4: TGroupBox;
    GroupBox5: TGroupBox;
    GroupBox6: TGroupBox;
    GroupBox7: TGroupBox;
    Image1: TImage;
    Image2: TImage;
    Image3: TImage;
    Image4: TImage;
    Label3: TLabel;
    Label4: TLabel;
    Serial: TLazSerial;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    RadioButton3: TRadioButton;
    RadioButton4: TRadioButton;
    RadioButton5: TRadioButton;
    RadioButton6: TRadioButton;
    RadioButton7: TRadioButton;
    RadioButton8: TRadioButton;
    Label1: TLabel;
    Label2: TLabel;
    Timer1: TTimer;

    procedure Chart1BeforeDrawBackWall(ASender: TChart; ACanvas: TCanvas;
      const ARect: TRect; var ADoDefaultDrawing: Boolean);
    procedure Chart2BeforeDrawBackWall(ASender: TChart; ACanvas: TCanvas;
      const ARect: TRect; var ADoDefaultDrawing: Boolean);
    procedure CheckBox1Change(Sender: TObject);

    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure RadioButton1Change(Sender: TObject);
    procedure RadioButton2Change(Sender: TObject);
    procedure RadioButton3Change(Sender: TObject);
    procedure RadioButton4Change(Sender: TObject);
    procedure RadioButton5Change(Sender: TObject);
    procedure RadioButton6Change(Sender: TObject);
    procedure RadioButton7Change(Sender: TObject);
    procedure RadioButton8Change(Sender: TObject);
    procedure SetTime(Sender: TObject);
    {function SendHttpData(Sender: TObject);}

    procedure SerialRxData(Sender: TObject);

  private
  public
  end;

var
  Form1: TForm1;
  Img_0: TPicture;
  Img_1: TPicture;
  station: integer;
  front: integer;
  a_mp01: array[0..10] of integer;
  a_mp25: array[0..10] of integer;
  a_mp10: array[0..10] of integer;

implementation

procedure clean_array();
var i: integer;
begin
  for i := 0 to 10 do
  begin
    a_mp01[i] := 0;
    a_mp25[i] := 0;
    a_mp10[i] := 0;
  end;
end;
{$R *.lfm}

procedure TForm1.FormCreate(Sender: TObject);
begin
  Img_0 := TPicture.Create;
  Img_1 := TPicture.Create;

  Img_0.LoadFromFile('img/bkg1.png');
  Img_1.LoadFromFile('img/bkg2.png');
  front := 0;

  station := 1;
  RadioButton1.checked := True;
  Timer1.Enabled := True;
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  Serial.Active := False;
  Serial.Close;
end;

procedure TForm1.RadioButton1Change(Sender: TObject);
begin
  station := 1;
  front := 0;
  SerieMp01.Clear;
  SerieMp25.Clear;
  SerieMp10.Clear;
  clean_array;
end;

procedure TForm1.RadioButton2Change(Sender: TObject);
begin
  station := 2;
  front := 0;
  SerieMp01.Clear;
  SerieMp25.Clear;
  SerieMp10.Clear;
  clean_array;
end;

procedure TForm1.RadioButton3Change(Sender: TObject);
begin
  station := 3;
  front := 0;
  SerieMp01.Clear;
  SerieMp25.Clear;
  SerieMp10.Clear;
  clean_array;
end;

procedure TForm1.RadioButton4Change(Sender: TObject);
begin
  station := 4;
  front := 0;
  SerieMp01.Clear;
  SerieMp25.Clear;
  SerieMp10.Clear;
  clean_array;
end;

procedure TForm1.RadioButton5Change(Sender: TObject);
begin
  station := 5;
  front := 0;
  SerieMp01.Clear;
  SerieMp25.Clear;
  SerieMp10.Clear;
  clean_array;
end;

procedure TForm1.RadioButton6Change(Sender: TObject);
begin
  SerieMp01.Marks.Visible := True;
  SerieMp25.Marks.Visible := False;
  SerieMp10.Marks.Visible := False;
end;

procedure TForm1.RadioButton7Change(Sender: TObject);
begin
  SerieMp01.Marks.Visible := False;
  SerieMp25.Marks.Visible := True;
  SerieMp10.Marks.Visible := False;
end;

procedure TForm1.RadioButton8Change(Sender: TObject);
begin
  SerieMp01.Marks.Visible := False;
  SerieMp25.Marks.Visible := False;
  SerieMp10.Marks.Visible := True;
end;

procedure TForm1.Chart1BeforeDrawBackWall(Asender: TChart; ACanvas: TCanvas;
  const ARect: TRect; var ADoDefaultDrawing: boolean);
begin
  ACanvas.StretchDraw(ARect, Img_0.Graphic);
  ADoDefaultDrawing := False;
end;


procedure TForm1.Chart2BeforeDrawBackWall(ASender: TChart; ACanvas: TCanvas;
  const ARect: TRect; var ADoDefaultDrawing: Boolean);
begin
  ACanvas.StretchDraw(ARect, Img_1.Graphic);
  ADoDefaultDrawing := False;
end;

procedure TForm1.CheckBox1Change(Sender: TObject);
begin
  if Serial.Active then
  begin
    Serial.Active := False;
  end
  else
  begin
    Serial.Active := True;
  end;
end;



{Esto funciona con el componente TTimer y su evento OnTimer}

procedure TForm1.SetTime(Sender: TObject);
begin
  Label1.Caption := 'Fecha: ' + FormatDateTime('dd/mm/yyyy', Now) +
      ' - Hora: ' + FormatDateTime('hh:nn:ss', Now);
end;


{http funcional usar postJson.Add para añádir la data del MP}
{Se ejecuta el post al clickear el checkbox "Envía" }

procedure SendHttpData(a_data: TStringDynArray);
var
  postJson: TJSONObject;
  responseBody: string;
begin

    postJson := TJSONObject.Create;
    postJson.Add('station', StrToInt(a_data[0]));
    postJson.Add('mp01', StrToInt(a_data[1]));
    postJson.Add('mp25', StrToInt(a_data[2]));
    postJson.Add('mp10', StrToInt(a_data[3]));
    postJson.Add('hist03', StrToInt(a_data[4]));
    postJson.Add('hist05', StrToInt(a_data[5]));
    postJson.Add('hist01', StrToInt(a_data[6]));
    postJson.Add('hist25', StrToInt(a_data[7]));
    postJson.Add('hist50', StrToInt(a_data[8]));
    postJson.Add('hist10', StrToInt(a_data[9]));
    postJson.Add('temp', StrToInt(a_data[10]));
    postJson.Add('hum', StrToInt(a_data[11]));

    with TFPHttpClient.Create(nil) do
      try
        AddHeader('Content-Type', 'application/json');
        RequestBody := TStringStream.Create(postJson.AsJSON);
        responseBody := Post('http://localhost:7070/data');
        //Label2.Caption := responseBody;
      finally
        Free;
      end;
end;

procedure TForm1.SerialRxData(Sender: TObject);
var
  s_data: string;
  a_item: TStringDynArray;
  i: integer;
  pos: integer;
  index: integer;
begin
  s_data := Serial.ReadData;
  a_item := SplitString(s_data, ',');

  if StrToInt(a_item[0]) = station then
  begin
    Label2.Caption := Concat('Data: ', s_data);

    Histograma.Clear;

    Histograma.AddXY(0, StrToInt(a_item[4]), a_item[4], clRed);
    Histograma.AddXY(1, StrToInt(a_item[5]), a_item[5], clGreen);
    Histograma.AddXY(2, StrToInt(a_item[6]), a_item[6], clBlue);
    Histograma.AddXY(3, StrToInt(a_item[7]), a_item[7], clYellow);
    Histograma.AddXY(4, StrToInt(a_item[8]), a_item[8], clLime);
    Histograma.AddXY(5, StrToInt(a_item[9]), a_item[9], clGray);

    Label3.Caption := a_item[10];
    Label4.Caption := a_item[11];
    if CheckBox2.checked = True then
    begin
      SendHttpData(a_item);
    end;
    if SerieMp01.count < 11 then
    begin
      index := 0;
      pos := 10 - front;
      while pos < 10 do
      begin
        if pos = (10 - front) then
        begin
          SerieMp01.Clear;
          SerieMp25.Clear;
          SerieMp10.Clear;
        end;

        SerieMp01.AddXY(pos, a_mp01[index], IntToStr(a_mp01[index]));
        SerieMp25.AddXY(pos, a_mp25[index], IntToStr(a_mp25[index]));
        SerieMp10.AddXY(pos, a_mp10[index], IntToStr(a_mp10[index]));
        index := index + 1;
        pos := pos + 1;
      end;
      a_mp01[front] := StrToInt(a_item[1]);
      a_mp25[front] := StrToInt(a_item[2]);
      a_mp10[front] := StrToInt(a_item[3]);

      front := front + 1;

      SerieMp01.AddXY(10, StrToInt(a_item[1]), a_item[1]);
      SerieMp25.AddXY(10, StrToInt(a_item[2]), a_item[2]);
      SerieMp10.AddXY(10, StrToInt(a_item[3]), a_item[3]);
    end
    else
    begin
      front := (front + 1) mod 11;

      a_mp01[front] := StrToInt(a_item[1]);
      a_mp25[front] := StrToInt(a_item[2]);
      a_mp10[front] := StrToInt(a_item[3]);
      SerieMp01.Clear;
      SerieMp25.Clear;
      SerieMp10.Clear;
      for i := front to 10 + front do
      begin
        SerieMp01.AddXY(i - front, a_mp01[i mod 11], IntToStr(a_mp01[i mod 11]));
        SerieMp25.AddXY(i - front, a_mp25[i mod 11], IntToStr(a_mp25[i mod 11]));
        SerieMp10.AddXY(i - front, a_mp10[i mod 11], IntToStr(a_mp10[i mod 11]));
      end;
    end;

  end;


end;

end.

